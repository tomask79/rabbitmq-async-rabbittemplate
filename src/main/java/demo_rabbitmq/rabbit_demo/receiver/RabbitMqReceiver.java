package demo_rabbitmq.rabbit_demo.receiver;

public class RabbitMqReceiver {

	public String onMessage(String personName) {
		try {
			System.out.println("Doing something at receive with payload["+personName+"] ");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "Hello from server: "+personName;
	}
}
