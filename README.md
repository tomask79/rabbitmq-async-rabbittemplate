# Spring AMQP with RabbitMQ, part 5 #

## AsyncRabbitTemplate and asynchronous callback ##

During the preparation of the RabbitMQ training for my colleagues I noticed that since RabbitMQ version 1.6 we can use
AsyncRabbitTemplate allowing asynchronous callbacks for processing the response in request/response communication between systems. Lets test it:

## Prerequisities (infrastructure on RabbitMQ) ##

* **Request**
* Exchange: test_exchange (Direct)
* Queue: greeting with routing key "greeting"

* **Response**
* Exchange: reply_exchange (Direct)
* Queue: replies 


## Bean configurations ##

```
@Bean
public AsyncRabbitTemplate asyncRabbitTemplate(final ConnectionFactory connectionFactory) {
	return new AsyncRabbitTemplate(connectionFactory, "test_exchange","greeting",
			"replies", "reply_exchange/replies");
}
```
*Requests send through AsyncRabbitTemplate will be routed to test_exchange with "greeting" routing key. Replies will be then sent back to reply_exchange/replies address.*   

```
@Bean
public MessageListener receiver() {
    return new MessageListenerAdapter(new RabbitMqReceiver(), "onMessage");
}

@Bean
public SimpleMessageListenerContainer serviceListenerContainer(
		ConnectionFactory connectionFactory) {
	SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
	container.setConnectionFactory(connectionFactory);
	container.setQueues(greeting());
	container.setMessageListener(receiver());
	return container;
}
```
SimpleMessageListenerContainer configured to listen on the "greeting" queue to process the requests in the receiver() adapter. Replies will be sent to location set by "replyTO" header which is filled with address "reply_exchange/replies" by AsyncRabbitTemplate.

## AsyncRabbitTemplate with asynchronous callback in action ##

**Message processing receiver**

```
public class RabbitMqReceiver {

	public String onMessage(String personName) {
		try {
			System.out.println("Doing something at receive with payload["+personName+"] ");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return "Hello from server: "+personName;
	}
}
```

**Sending the message with asynchronous response**:
```
/**
 * @author Tomas Kloucek
 */
public class RabbitMqSender {

	@Autowired
	private AsyncRabbitTemplate asyncRabbitTemplate;
	
	public void send() {
		Message requestMessage = MessageBuilder.withBody("Tomas Kloucek".getBytes())
				.setContentType("text/plain")
				.build();

		requestMessage.getMessageProperties().setMessageId(UUID.randomUUID().toString());
		System.out.println("I'm about to send message with ID: ["+requestMessage.getMessageProperties().getMessageId()+" ]");
		AsyncRabbitTemplate.RabbitMessageFuture reply = this.asyncRabbitTemplate.sendAndReceive(requestMessage);
		reply.addCallback(new ListenableFutureCallback<Message>() {
			@Override
			public void onFailure(Throwable throwable) {
				throwable.printStackTrace();
			}

			@Override
			public void onSuccess(Message message) {
				System.out.println("Server returned: "+new String(message.getBody()));
			}
		});
		System.out.println("Send method end.");
	}
```

## Testing the demo ##

* git clone <this repo>
* mvn clean install
* java -jar target/demo-0.0.1-SNAPSHOT.jar

**Output:**

```
I'm about to send message with ID: [d12e512f-c228-4955-8863-509af955479c ]
Send method end.
Doing something at receive with payload[Tomas Kloucek]
Server returned: Hello from server: Tomas Kloucek
```